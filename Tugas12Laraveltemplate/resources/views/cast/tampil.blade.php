@extends('layouts.master')

@section('title')
 Halaman tampil data
@endsection

@section('content')

<a href="/cast/create" class="btn btn-sm btn-primary">tambah</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">pemain</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row"{{$key + 1}}</th>
            <td>{{$item->nama}}</td>
            <td>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">detail</a>
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-warning">edit</a>
            </td>
          </tr> 
        @empty
            cast belum ada
        @endforelse
     
    </tbody>
  </table>

@endsection
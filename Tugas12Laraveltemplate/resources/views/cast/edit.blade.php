@extends('layouts.master')

@section('title')
 Halaman edit data
@endsection

@section('content')

<form method="POST" action="/cast/{{$cast->id}}">
    @if ($errors->any())
    <div class="alert alert-danger">
         <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
         </ul>
    </div>
  @endif
  @csrf
  @method("put")
  <div class="form-group">
    <label >nama</label>
    <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" >
  </div>
  <div class="form-group">
    <label >umur</label>
    <input type="text" class="form-control" name="umur">
  </div>
  <div class="form-group">
    <label >bio</label>
    <textarea name="bio" class="form-contol" cols="30" rows="10">{{$cast->description}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
      $Cast = cast::all();
      
      return view('cast.tampil', ['cast'=> $cast]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view ('cast.create' );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required min:5',
            'umur' => 'required',
        ],
        [
            'nama' => 'nama harus diisi tidak boleh kosong',
            'umur' => 'umur harus diisi tidak boleh kosong',

        ]);

        $Cast = new cast;

        $Cast->name = $request->input('nama');
        $Cast->umur = $request->input('umur');

        $cast->save();

        return redirect('/cast');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Cast = cast::find($id);

        return view('cast.detail', ['cast'=> $cast]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
